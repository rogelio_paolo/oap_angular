import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdSubMenuComponent } from './third-sub-menu.component';

describe('ThirdSubMenuComponent', () => {
  let component: ThirdSubMenuComponent;
  let fixture: ComponentFixture<ThirdSubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdSubMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
