import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstSubMenuComponent } from './first-sub-menu.component';

describe('FirstSubMenuComponent', () => {
  let component: FirstSubMenuComponent;
  let fixture: ComponentFixture<FirstSubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstSubMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
