import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondSubMenuComponent } from './second-sub-menu.component';

describe('SecondSubMenuComponent', () => {
  let component: SecondSubMenuComponent;
  let fixture: ComponentFixture<SecondSubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondSubMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
