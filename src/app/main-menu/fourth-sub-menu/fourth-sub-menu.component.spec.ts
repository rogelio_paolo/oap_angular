import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourthSubMenuComponent } from './fourth-sub-menu.component';

describe('FourthSubMenuComponent', () => {
  let component: FourthSubMenuComponent;
  let fixture: ComponentFixture<FourthSubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourthSubMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourthSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
