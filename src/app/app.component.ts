import { Component } from '@angular/core';
import {VERSION} from '@angular/material';
import {NavItem} from './main-menu/nav-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  version = VERSION;
  navItems: NavItem[] = [
    {
      displayName: '',
      iconName: '',
    },
    {
      displayName: 'Accounts',
      iconName: 'close',
      children: [
        {
          displayName: 'All',
          iconName: 'library_books',
          route: 'account-list'
        },
      ]
    },
    {
      displayName: 'Campaigns',
      disabled: true,
      iconName: 'close',
      children: [
        {
          displayName: 'All',
          iconName: 'recored_voice_over',
        },
      ]
    },
    {
      displayName: 'AdGroups',
      disabled: true,
      iconName: 'close',
      children: [
        {
          displayName: 'All',
          iconName: 'close',
        },
      ]
    },
    {
      displayName: 'Keywords',
      disabled: true,
      iconName: 'close',
      children: [
        {
          displayName: 'All',
          iconName: 'close',
        },
      ]
    },
    {
      displayName: 'Report',
      disabled: true,
      iconName: 'close',
      children: [
        {
          displayName: 'All',
          iconName: 'close',
        },
      ]
    }
  ];
}
