import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FirstSubMenuComponent} from './main-menu/first-sub-menu/first-sub-menu.component';
import {SecondSubMenuComponent} from './main-menu/second-sub-menu/second-sub-menu.component';
import {ThirdSubMenuComponent} from './main-menu/third-sub-menu/third-sub-menu.component';
import {FourthSubMenuComponent} from './main-menu/fourth-sub-menu/fourth-sub-menu.component';
import {ListComponent} from './account/list/list.component';

const routes: Routes = [

  {path: '', component: FirstSubMenuComponent, pathMatch: 'full'},
  {path: 'account-list', component: ListComponent},
  {path: 'what-up-web', component: SecondSubMenuComponent},
  {path: 'my-ally-cli', component: ThirdSubMenuComponent},
  {path: 'become-angular-tailer', component: FourthSubMenuComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
