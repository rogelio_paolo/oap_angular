# OapFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Getting the repository in local/hosting server

0) git init
1) git remote add remotename git@bitbucket.org:rogelio_paolo/oap_angular.git
2) ssh-keygen -t rsa
3) eval $(ssh-agent)
4) cat ~/path/to/key/yourkeyname then paste this key to in settings/access keys/add new
4) ssh-add ~/path/to/key/yourkeyname
5) if master branch:
- git fetch && git checkout master
else
- git fetch -all
- git reset --hard remotename/branchname
- git pull remotename branchname